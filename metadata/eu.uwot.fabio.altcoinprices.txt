Categories:Money
License:GPL-3.0
Web Site:https://uwot.eu/blog/altcoin-prices
Source Code:https://gitlab.com/cfabio/AltcoinPrices/tree/HEAD
Issue Tracker:https://gitlab.com/cfabio/AltcoinPrices/issues
Changelog:https://gitlab.com/cfabio/AltcoinPrices/tags
Bitcoin:353x3kNMUaAt3i79kQTf3KCJWRAVXSRGpW

Auto Name:Altcoin Prices
Summary:Monitor altcoin prices in real time
Description:
Cryptocurrencies portfolio tracker, monitor prices in real time and much more.
Data are retrieved directly using exchange's API for maximum privacy.
.

Repo Type:git
Repo:https://gitlab.com/cfabio/AltcoinPrices.git

Build:0.3b,1
    commit=0.3b
    subdir=Altcoin Prices
    gradle=yes

Build:0.3c,2
    commit=0.3c
    subdir=Altcoin Prices
    gradle=yes

Build:0.3d,3
    commit=0.3d
    subdir=Altcoin Prices
    gradle=yes

Build:0.3e,4
    commit=0.3e
    subdir=Altcoin Prices
    gradle=yes

Build:0.3f,5
    commit=0.3f
    subdir=Altcoin Prices
    gradle=yes

Build:0.3h,7
    commit=0.3h
    subdir=Altcoin Prices
    gradle=yes

Build:1.2,11
    commit=1.2
    subdir=Altcoin Prices
    gradle=yes

Build:1.3.1,13
    commit=1.3.1
    subdir=Altcoin Prices
    gradle=yes

Build:1.3.3,15
    commit=1.3.3
    subdir=Altcoin Prices
    gradle=yes

Build:1.3.5,17
    commit=1.3.5
    subdir=Altcoin Prices
    gradle=yes

Build:1.4.3,20
    commit=1.4.3
    subdir=Altcoin Prices
    gradle=yes

Build:1.4.5a,23
    commit=1.4.5a
    subdir=Altcoin Prices
    gradle=yes

Build:1.4.6,24
    commit=1.4.6
    subdir=Altcoin Prices
    gradle=yes

Build:1.4.8a,26
    commit=1.4.8a
    subdir=Altcoin Prices
    gradle=yes

Build:1.4.9,28
    commit=1.4.9
    subdir=Altcoin Prices
    gradle=yes

Build:1.4.10,29
    commit=1.4.10
    subdir=Altcoin Prices
    gradle=yes

Build:1.4.11c,32
    commit=1.4.11c
    subdir=Altcoin Prices
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.4.11c
Current Version Code:32
